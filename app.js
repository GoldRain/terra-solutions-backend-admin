const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId


const app = express();

// load route
const projectmanager = require('./routes/projectmanager');
const machinedata = require('./routes/machinedata');
const projectname = require('./routes/projectname');
const operater = require('./routes/operater');
const machine = require('./routes/machine');
const admin = require('./routes/admin');

mongoose.connect('mongodb://localhost/terr-mongo', {
        useNewUrlParser: true
    }).then(() => console.log('mongodb Connected..'))
    .catch(err => console.log(err))


// Body Parser middleware
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Request-Headers','*');
    res.setHeader('Access-Control-Request-Method','*');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/public',express.static('./public'));

app.use('/privacyPolicy',express.static(path.join(__dirname, '/privacyPolicy.html')))

app.use(express.static(path.join(__dirname, 'terr-admin')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'terr-admin/index.html'))
})


const port = 8500;
// const port = 5000;

app.use('/projectmanager', projectmanager);
app.use('/projectname', projectname);
app.use('/operater', operater);
app.use('/machine', machine);
app.use('/machinedata', machinedata);
app.use('/admin', admin);

app.listen(port, () => {
    console.log(`Server start at ${port}`)
})