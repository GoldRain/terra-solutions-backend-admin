const express = require('express');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId
const router = express.Router();
const jwt = require('jsonwebtoken');

const {
    verifyToken
} = require('../helpers/auth');

require('../models/Operater');
const Operater = mongoose.model('operaters');

require('../models/Assignedoperater');
const Assignedoperater = mongoose.model('assignedoperaters');

module.exports = router;

// Add operater
router.post('/add', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("Operater Add")
            const newOp = {
                name: req.body.name
            }
            new Operater(newOp)
                .save()
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})


// assigned machine to Operater
router.post('/assignMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log('Assigned machine and operater')
            Assignedoperater.findOne({
                operaterId: ObjectId(req.body.operaterId),
                machineId: ObjectId(req.body.machineId)
            }, (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else if (data) {
                    res.json({
                        error: true,
                        message: "Already linked"
                    })
                } else {
                    const newAssign = {
                        operaterId: ObjectId(req.body.operaterId),
                        machineId: ObjectId(req.body.machineId)
                    }
                    new Assignedoperater(newAssign)
                        .save()
                        .then(data => {
                            res.json({
                                error: false,
                                data: data
                            })
                        })
                        .catch(err => {
                            res.json({
                                error: true,
                                message: err
                            })
                        })
                }
            })
        }
    })
})

// find operater with respect to Machine
router.post('/operaterRespectMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("operaterRespectMachine");

            Assignedoperater.aggregate([{
                $match: {
                    $and: [{
                            machineId: ObjectId(req.body.machineId)
                        },
                        {
                            isActive: true
                        }
                    ]
                }
            }, {
                $lookup: {
                    from: "operaters",
                    localField: "operaterId",
                    foreignField: "_id",
                    as: "operaterData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// find deactive operater with respect to Machine
router.post('/deactiveOperaterRespectMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("deactiveOperaterRespectMachine");

            Assignedoperater.aggregate([{
                $match: {
                    $and: [{
                            machineId: ObjectId(req.body.machineId)
                        },
                        {
                            isActive: false
                        }
                    ]
                }
            }, {
                $lookup: {
                    from: "operaters",
                    localField: "operaterId",
                    foreignField: "_id",
                    as: "operaterData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// Active operater and Machine
router.post('/activateLinkOperater', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activatelinkOperater");
            Assignedoperater.updateOne({
                    machineId: ObjectId(req.body.machineId),
                    operaterId: ObjectId(req.body.operaterId)
                }, {
                    $set: {
                        isActive: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// de link operater and Machine
router.post('/deLinkOperater', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("delinkOperater");
            Assignedoperater.updateOne({
                    machineId: ObjectId(req.body.machineId),
                    operaterId: ObjectId(req.body.operaterId)
                }, {
                    $set: {
                        isActive: false
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get all Operater List
router.post('/allOperaterList', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Operater");
            Operater.find({
                    isActive: true
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get all Operater
router.post('/allOperater', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Operater");
            let word = req.body.word;
            let number = req.body.number;
            let query = number * 15;
            Operater.find({
                    name: {
                        $regex: ".*" + word + ".*",
                        $options: 'si'
                    },
                    isActive: true
                }).skip(query).limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get Blacklist operator
router.post('/blacklist', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("blacklist operator");
            let number = req.body.number;
            let query = number * 15;
            Operater.find({
                    isActive: false
                }).skip(query).limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get one operater
router.post('/getOneOperater', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("get one operater")
            Operater.findOne({
                    _id: ObjectId(req.body.id)
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// deactivate Operater
router.post('/deactivate', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log('deactivate Operater')
            Operater.updateOne({
                _id: ObjectId(req.body.id)
            }, {
                $set: {
                    isActive: false
                }
            }, (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else {
                    Assignedoperater.updateMany({
                        operaterId: ObjectId(req.body.id)
                    }, {
                        $set: {
                            isActive: false
                        }
                    }, (err, data) => {
                        if (err) {
                            res.json({
                                error: true,
                                message: err
                            })
                        } else {
                            res.json({
                                error: false,
                                message: "Success"
                            })
                        }
                    })
                }
            })
        }
    })
})

// Activate operater
router.post('/activate', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log('activate operater')
            Operater.updateOne({
                    _id: ObjectId(req.body.id)
                }, {
                    $set: {
                        isActive: true
                    }
                }).then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})


// delink machine
router.post('/delinkMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("delink Machine");
            Assignedoperater.updateOne({
                    operaterId: ObjectId(req.body.operaterId),
                    machineId: ObjectId(req.body.machineId)
                }, {
                    $set: {
                        isActive: false
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// activateLink machine
router.post('/activateLinkMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activateLink Machine");
            Assignedoperater.updateOne({
                    operaterId: ObjectId(req.body.operaterId),
                    machineId: ObjectId(req.body.machineId)
                }, {
                    $set: {
                        isActive: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// count Operater
router.post('/count', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("count Operater");
            Operater.aggregate([{
                    $match: {
                        isActive: true
                    }
                }, {
                    $count: "count"
                }])
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// find machine with respect to operater
router.post('/machineRespectOperater', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("machineRespectOperater");

            Assignedoperater.aggregate([{
                $match: {
                    $and: [{
                            operaterId: ObjectId(req.body.operaterId)
                        },
                        {
                            isActive: true
                        }
                    ]
                }
            }, {
                $lookup: {
                    from: "machines",
                    localField: "machineId",
                    foreignField: "_id",
                    as: "machineData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// find machine with respect to operater
router.post('/deactiveMachineRespectOperater', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("deactiveMachineRespectOperater");

            Assignedoperater.aggregate([{
                $match: {
                    $and: [{
                            operaterId: ObjectId(req.body.operaterId)
                        },
                        {
                            isActive: false
                        }
                    ]
                }
            }, {
                $lookup: {
                    from: "machines",
                    localField: "machineId",
                    foreignField: "_id",
                    as: "machineData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// update OperaterName
router.post('/updateOperaterName', verifyToken, (req,res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("updateOperaterName")
            Operater.updateOne({
                _id:ObjectId(req.body.id)
            },{
                $set:{
                    name:req.body.name
                }
            })
            .then(data => {
                res.json({
                    error:false,
                    data:data
                })
            })
            .catch(err => {
                res.json({
                    error:true,
                    message:err
                })
            })
        }
    })
})