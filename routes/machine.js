const express = require('express');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId
const router = express.Router();
const jwt = require('jsonwebtoken');

const {
    verifyToken
} = require('../helpers/auth');

require('../models/Machine');
const Machine = mongoose.model('machines');

require('../models/Assignedmachine');
const Assignedmachine = mongoose.model('assignedmachines');

require('../models/Assignedoperater');
const Assignedoperater = mongoose.model('assignedoperaters')

module.exports = router;

// Add machine
router.post('/add', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("Machine Add")
            const newOp = {
                name: req.body.name
            }
            new Machine(newOp)
                .save()
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// find machine with respect to project manager
router.post('/machineRespectManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("machineRespectManager");
            let number = req.body.number;
            let query = number * 15;
            Assignedmachine.aggregate([{
                    $match: {
                        $and: [{
                                managerId: ObjectId(req.body.managerId)
                            },
                            {
                                isActive: true
                            }
                        ]

                    }
                }, {
                    $lookup: {
                        from: "machines",
                        localField: "machineId",
                        foreignField: "_id",
                        as: "machineData"
                    }
                },
                {
                    "$skip": query
                },
                {
                    "$limit": 15
                }
            ], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        error: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// find Deactivate machine with respect to project manager
router.post('/deactivateMachineRespectManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("deactivateMachineRespectManager");
            let number = req.body.number;
            let query = number * 15;
            Assignedmachine.aggregate([{
                    $match: {
                        $and: [{
                                managerId: ObjectId(req.body.managerId)
                            },
                            {
                                isActive: false
                            }
                        ]

                    }
                }, {
                    $lookup: {
                        from: "machines",
                        localField: "machineId",
                        foreignField: "_id",
                        as: "machineData"
                    }
                },
                {
                    "$skip": query
                },
                {
                    "$limit": 15
                }
            ], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        error: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// get all machine List
router.post('/allMachineList', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Machine List");
            Machine.find({
                    active: true
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get all machine Pagination Regex
router.post('/allMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Machine");
            let word = req.body.word;
            let number = req.body.number;
            let query = number * 15;
            Machine.find({
                    name: {
                        $regex: ".*" + word + ".*",
                        $options: 'si'
                    },
                    active: true
                }).skip(query).limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

//get blacklist machines
router.post('/blacklist', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("blacklistMachine");
            let number = req.body.number;
            let query = number * 15;
            Machine.find({
                    active: false
                }).skip(query).limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// deactivate machine
router.post('/deactivate', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("deactivate machine");
            Machine.updateOne({
                _id: ObjectId(req.body.id)
            }, {
                $set: {
                    active: false
                }
            }, (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else {
                    Assignedmachine.updateMany({
                        machineId: ObjectId(req.body.id)
                    }, {
                        $set: {
                            isActive: false
                        }
                    }, (err, data) => {
                        if (err) {
                            res.json({
                                error: true,
                                message: err
                            })
                        } else {
                            Assignedoperater.updateMany({
                                machineId: ObjectId(req.body.id)
                            }, {
                                $set: {
                                    isActive: false
                                }
                            }, (err, data) => {
                                if (err) {
                                    res.json({
                                        error: true,
                                        message: err
                                    })
                                } else {
                                    res.json({
                                        error: false,
                                        message: "Success"
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
})

// activate Machine
router.post('/activate', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activate");
            Machine.updateOne({
                    _id: ObjectId(req.body.id)
                }, {
                    $set: {
                        active: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// count Machine
router.post('/count', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("count Machine");
            Machine.aggregate([{
                    $match: {
                        active: true
                    }
                }, {
                    $count: "count"
                }])
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get one machine
router.post('/getOneMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("getOneMachine")
            Machine.findOne({
                    _id: ObjectId(req.body.id)
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// find project manager with respect to machine
router.post('/managerRespectMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("managerRespectMachine");
            Assignedmachine.aggregate([{
                $match: {
                    $and: [{
                            machineId: ObjectId(req.body.machineId)
                        },
                        {
                            isActive: true
                        }
                    ]

                }
            }, {
                $lookup: {
                    from: "projectmanagers",
                    localField: "managerId",
                    foreignField: "_id",
                    as: "managerData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        error: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// find project manager with respect to machine
router.post('/deactiveManagerRespectMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("deactiveManagerRespectMachine");
            Assignedmachine.aggregate([{
                $match: {
                    $and: [{
                            machineId: ObjectId(req.body.machineId)
                        },
                        {
                            isActive: false
                        }
                    ]

                }
            }, {
                $lookup: {
                    from: "projectmanagers",
                    localField: "managerId",
                    foreignField: "_id",
                    as: "managerData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        error: err
                    })
                } else {
                    res.json({
                        error: false,
                        data: data
                    })
                }
            })
        }
    })
})

// de Link manager and Machine
router.post('/deLinkManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("delinkManger");
            Assignedmachine.updateOne({
                    machineId: ObjectId(req.body.machineId),
                    managerId: ObjectId(req.body.managerId)
                }, {
                    $set: {
                        isActive: false
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// Active manager and Machine
router.post('/activateLinkManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activatelinkManger");
            Assignedmachine.updateOne({
                    machineId: ObjectId(req.body.machineId),
                    managerId: ObjectId(req.body.managerId)
                }, {
                    $set: {
                        isActive: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// update name
router.post('/updateMachineName', verifyToken, (req,res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            Machine.updateOne({
                _id:ObjectId(req.body.id)
            },{
                $set:{
                    name:req.body.name
                }
            })
            .then(data => {
                res.json({
                    error:false,
                    data:data
                })
            })
            .catch(err => {
                res.json({
                    error:true,
                    message:err
                })
            })
        }
    })
})