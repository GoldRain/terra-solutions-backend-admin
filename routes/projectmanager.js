const express = require("express");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const jwt = require("jsonwebtoken");
const router = express.Router();

const { verifyToken } = require("../helpers/auth");

require("../models/Projectmanager");
const Projectmanager = mongoose.model("projectmanagers");

require("../models/Assignedproject");
const Assignedproject = mongoose.model("assignedprojects");

require("../models/Assignedmachine");
const Assignedmachine = mongoose.model("assignedmachines");

module.exports = router;

// Add Projectmanager
router.post("/add", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("projectmanager Add");
            Projectmanager.find({ email: req.body.email }, (err, data) => {
                if (err) {
                    res.json({ error: true, message: err });
                } else if (data.length > 0) {
                    res.json({ error: true, message: "Email already exist" });
                } else {
                    const newPM = {
                        name: req.body.name,
                        email: req.body.email,
                        password: req.body.password
                    };
                    new Projectmanager(newPM)
                        .save()
                        .then(data => {
                            res.json({ error: false, data: data });
                        })
                        .catch(err => {
                            res.json({ error: true, message: err });
                        });
                }
            }
            );
        }
    })
});

// project to projectmanager
router.post("/assignProject", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("Assigning manager to project");
            Assignedproject.findOne({ projectId: ObjectId(req.body.projectId), managerId: ObjectId(req.body.managerId) }, (err, data) => {
                if (err) {
                    res.json({ error: true, message: err });
                } else if (data) {
                    res.json({ error: true, message: "Already linked" });
                } else {
                    const newAssign = {
                        projectId: ObjectId(req.body.projectId),
                        managerId: ObjectId(req.body.managerId)
                    };
                    new Assignedproject(newAssign).save()
                        .then(data => {
                            res.json({ error: false, data: data });
                        })
                        .catch(err => {
                            res.json({ error: true, message: err });
                        });
                }
            });
        }
    })
});

// projectmanager and machine
router.post("/assignMachine", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("Assigning machine to project Manager");
            Assignedmachine.findOne({ machineId: ObjectId(req.body.machineId), managerId: ObjectId(req.body.managerId) }, (err, data) => {
                if (err) {
                    res.json({ error: true, message: err });
                } else if (data) {
                    res.json({ error: true, message: "Already linked" });
                } else {
                    const newAssign = {
                        machineId: ObjectId(req.body.machineId),
                        managerId: ObjectId(req.body.managerId)
                    };
                    new Assignedmachine(newAssign)
                        .save()
                        .then(data => {
                            res.json({ error: false, data: data });
                        })
                        .catch(err => {
                            res.json({ error: true, message: err });
                        });
                }
            });
        }
    })
});

// projectManager Login
router.post("/login", (req, res) => {
    console.log("projectmanager/Login");

    Projectmanager.findOne({ email: req.body.email, password: req.body.password }).then(data => {
        if (data) {
            jwt.sign({ data }, "secretkey", (err, token) => {
                res.json({ error: false, message: "Success Login", data: data, token: token });
            });
        } else {
            res.json({ error: true, message: "No Record Found" });
        }
    })
        .catch(err => {
            res.json({ error: true, message: err });
        });
});

// get all Project Manager List
router.post("/allProjectmanagerList", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Projectmanager List");
            Projectmanager.find({ isBlocked: false }).then(data => {
                res.json({ error: false, data: data });
            })
                .catch(err => {
                    res.json({ error: true, message: err });
                });
        }
    })
});

// get all Project Manager
router.post("/allProjectmanager", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Projectmanager");
            let word = req.body.word;
            let number = req.body.number;
            let query = number * 15;
            Projectmanager.find({
                name: {
                    $regex: ".*" + word + ".*",
                    $options: "si"
                },
                isBlocked: false
            })
                .skip(query)
                .limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    });
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    });
                });
        }
    })
});

// get blacklist project Manager
router.post("/blacklist", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("blacklistProjectManager");
            let number = req.body.number;
            let query = number * 15;
            Projectmanager.find({
                isBlocked: true
            })
                .skip(query)
                .limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    });
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    });
                });
        }
    })
});

// change Password
router.post("/changePassword", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("changePassword projectmanager");
            Projectmanager.findOne({ id: req.body.id }).then(data => {
                data.password = req.body.password;
                data.save()
                    .then(data => {
                        res.json({ error: false, data: data });
                    })
                    .catch(err => {
                        res.json({ error: true, message: err });
                    });
            })
                .catch(err => {
                    res.json({ error: true, message: false });
                });
        }
    })
});

// Block Project Manager
router.post("/block", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("Block Project Manager");
            Projectmanager.updateOne({
                _id: ObjectId(req.body.id)
            }, {
                    $set: {
                        isBlocked: true
                    }
                },
                (err, data) => {
                    if (err) {
                        res.json({
                            error: true,
                            message: error
                        });
                    } else {
                        Assignedmachine.updateMany({
                            managerId: ObjectId(req.body.id)
                        }, {
                                $set: {
                                    isActive: false
                                }
                            },
                            (err, data) => {
                                if (err) {
                                    res.json({
                                        error: true,
                                        message: error
                                    });
                                } else {
                                    Assignedproject.updateMany({
                                        managerId: ObjectId(req.body.id)
                                    }, {
                                            $set: {
                                                isActive: false
                                            }
                                        },
                                        (err, data) => {
                                            if (err) {
                                                res.json({
                                                    error: true,
                                                    message: err
                                                });
                                            } else {
                                                res.json({
                                                    error: false,
                                                    message: "Success"
                                                });
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    }
                }
            );
        }
    })
});

// unblock projectManager
router.post("/unBlock", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("unBlock Projectmanager");
            Projectmanager.updateOne({
                _id: ObjectId(req.body.id)
            }, {
                    $set: {
                        isBlocked: false
                    }
                },
                (err, data) => {
                    if (err) {
                        res.json({
                            error: true,
                            message: err
                        });
                    } else {
                        res.json({
                            error: false,
                            message: "Success"
                        });
                    }
                }
            );
        }
    })
});

// delink project
router.post("/delinkProject", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("delink Project");
            Assignedproject.updateOne({
                projectId: ObjectId(req.body.projectId),
                managerId: ObjectId(req.body.managerId)
            }, {
                    $set: {
                        isActive: false
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    });
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    });
                });
        }
    })
});

// delink machine
router.post("/delinkMachine", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("delink Machine");
            Assignedmachine.updateOne({
                machineId: ObjectId(req.body.machineId),
                managerId: ObjectId(req.body.managerId)
            }, {
                    $set: {
                        isActive: false
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    });
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    });
                });
        }
    })
});

// activeLink project
router.post("/activeLinkProject", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activeLinkProject");
            Assignedproject.updateOne({
                projectId: ObjectId(req.body.projectId),
                managerId: ObjectId(req.body.managerId)
            }, {
                    $set: {
                        isActive: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    });
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    });
                });
        }
    })
});

// activeLink Machine
router.post("/activeLinkMachine", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activeLinkMachine");
            Assignedmachine.updateOne({
                machineId: ObjectId(req.body.machineId),
                managerId: ObjectId(req.body.managerId)
            }, {
                    $set: {
                        isActive: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    });
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    });
                });
        }
    })
});

// count ProjectManager
router.post("/count", verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("count projectmanager");
            Projectmanager.aggregate([{
                $match: {
                    isBlocked: false
                }
            },
            {
                $count: "count"
            }
            ])
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    });
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    });
                });
        }
    })
});

// get one projectManager Detail
router.post('/getDetail', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log('getDetail projectmanager')
            Projectmanager.findOne({
                _id: req.body.id
            })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// project name update
router.post('/updateProjectManagerName', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("updateProjectManagerName");
            Projectmanager.updateOne({
                _id: ObjectId(req.body.id)
            }, {
                    $set: {
                        name: req.body.name
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})