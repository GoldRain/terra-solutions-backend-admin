const express = require('express');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId
const router = express.Router();
const path = require('path')
const multer = require('multer');
const jwt = require('jsonwebtoken');

const {
    verifyToken
} = require('../helpers/auth');

require('../models/Machinedata');
const Machinedata = mongoose.model("machinedatas")

require('../models/Machine');
const Machine = mongoose.model('machines');

router.use(express.static('./public'));

// Set Storage Engine
const storage_image = multer.diskStorage({
    destination:'./public/uploads',
    filename: function(req,file,cb){
        cb(null,file.fieldname   + '-' + Date.now() + path.extname(file.originalname));
    }
})

const upload = multer({
    storage : storage_image,
    fileFilter: function (req, file, cb) {
        checkFileType(file, cb)
    }
})

// check File Type
function checkFileType(file, cb) {
    // allowed ext
    const fileTypes = /jpeg|jpg|png|gif|bmp/;
    // check ext
    const extname = fileTypes.test(path.extname(file.originalname).toLowerCase());
    // check mime type
    const mimetype = fileTypes.test(file.mimetype);

    if (mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: Images Only');
    }
}

module.exports = router;

router.post('/add', verifyToken, upload.fields([{ name: 'image', maxCount: 1 }, { name: 'damageImg', maxCount: 1 }]), (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log(req.files.damageImg);
            if (req.files.damageImg) {
                damageImg = `/${req.files.damageImg[0].path}`;
            } else {
                damageImg = ""
            }
            if (req.files.image) {
                image = `/${req.files.image[0].path}`;
            } else {
                image = ""
            }
            console.log("add Machine data")
            let locationArr = [];
            let latitude = parseFloat(req.body.latitude);
            let longitude = parseFloat(req.body.longitude);
            locationArr.push(longitude);
            locationArr.push(latitude);
            const newData = {
                machineId: ObjectId(req.body.machineId),
                machineName: req.body.machineName,
                projectId: ObjectId(req.body.projectId),
                projectName: req.body.projectName,
                operaterId: ObjectId(req.body.operaterId),
                operaterName: req.body.operaterName,
                projectManagerId: ObjectId(req.body.projectManagerId),
                projectManagerName: req.body.projectManagerName,
                status: req.body.status,
                "location.coordinates": locationArr,
                place: req.body.place,
                hourmeter: req.body.hourmeter,
                image: image,
                clean: req.body.clean,
                greased: req.body.greased,
                engineOil: req.body.engineOil,
                hydraulicOil: req.body.hydraulicOil,
                fuel: req.body.fuel,
                damage: req.body.damage,
                noise: req.body.noise,
                description: req.body.description,
                damageImg: damageImg,
                description_damage:req.body.description_damage,
                created_at:Date.now()
            }
            new Machinedata(newData)
                .save()
                .then(data => {
                    Machine.findOne({
                            _id: ObjectId(req.body.machineId)
                        })
                        .then(check => {
                            check.status = req.body.status;
                            check.save()
                                .then(updated => {
                                    res.json({
                                        error: false,
                                        data: data
                                    })
                                })
                                .catch(err => {
                                    res.json({
                                        error: true,
                                        message: err
                                    })
                                })
                        })
                })
        }
    })
})


// machine Record with Regex
router.post('/recordRegex', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("recordRegex");
            let number = req.body.number;
            let query = number * 9;
            Machinedata.aggregate([{
                    $match: {
                        $and: [{
                            machineName: {
                                $regex: ".*" + req.body.machineName + ".*",
                                $options: 'si'
                            },
                        }, {
                            projectName: {
                                $regex: ".*" + req.body.projectName + ".*",
                                $options: 'si'
                            }
                        }, {
                            operaterName: {
                                $regex: ".*" + req.body.operaterName + ".*",
                                $options: 'si'
                            }
                        }, {
                            projectManagerName: {
                                $regex: ".*" + req.body.projectManagerName + ".*",
                                $options: 'si'
                            }
                        }]
                    }
                }, {
                    $sort: {
                        created_at: -1
                    }
                }, {
                    "$skip": query
                }, {
                    "$limit": 9
                }])
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// machine Record with Regex
router.post('/recordList', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("recordList");
            Machinedata.aggregate([{
                $match:{
                    machineId:ObjectId(req.body.machineId)
                }
            },{
                    $sort: {
                        created_at: -1
                    }
                }])
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get one Record
router.post('/getOneRecord', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log('getOneRecord machineData');
            Machinedata.findOne({
                    _id: ObjectId(req.body.id)
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        messsage: err
                    })
                })
        }
    })
})

// Repaired Fix
router.post('/repaireMachine', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log('repaireMachine machinedata');
            Machinedata.update({
                    _id: ObjectId(req.body.id)
                }, {
                    $set: {
                        repaired: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// machine Record 
router.post('/recordListDashboard', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("recordList");
            Machinedata.aggregate([{
                    $sort: {
                        created_at: -1
                    }
                }])
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})