const express = require('express');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId
const router = express.Router();
const jwt = require('jsonwebtoken');

const {
    verifyToken
} = require('../helpers/auth');

require('../models/Projectname');
const Projectname = mongoose.model('projectnames');

require('../models/Assignedproject');
const Assignedproject = mongoose.model('assignedprojects');

module.exports = router;

// Add Projectname
router.post('/add', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("Projectname Add")
            const newPN = {
                name: req.body.name
            }
            new Projectname(newPN)
                .save()
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// find project with respect to manager
router.post('/projectRespectManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("projectRespectManager");
            Assignedproject.aggregate([{
                $match: {
                    $and: [{
                            managerId: ObjectId(req.body.managerId)
                        },
                        {
                            isActive: true
                        }
                    ]

                }
            }, {
                $lookup: {
                    from: "projectnames",
                    localField: "projectId",
                    foreignField: "_id",
                    as: "projectData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        "error": true,
                        "message": err
                    })
                } else {
                    res.json({
                        "error": false,
                        "data": data
                    })
                }
            })
        }
    })
})

// find Deactivate project with respect to manager
router.post('/deactivateProjectRespectManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("DeactivateProjectRespectManager");
            Assignedproject.aggregate([{
                $match: {
                    $and: [{
                            managerId: ObjectId(req.body.managerId)
                        },
                        {
                            isActive: false
                        }
                    ]

                }
            }, {
                $lookup: {
                    from: "projectnames",
                    localField: "projectId",
                    foreignField: "_id",
                    as: "projectData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        "error": true,
                        "message": err
                    })
                } else {
                    res.json({
                        "error": false,
                        "data": data
                    })
                }
            })
        }
    })
})

// get all ProjectnameList
router.post('/allProjectnameList', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Projectname List");
            Projectname.find({
                    inProcess: true
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get all Projectname
router.post('/allProjectname', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("all Projectname");
            let word = req.body.word;
            let number = req.body.number;
            let query = number * 15;
            Projectname.find({
                    name: {
                        $regex: ".*" + word + ".*",
                        $options: 'si'
                    },
                    inProcess: true
                }).skip(query).limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

//get blacklist project Name
router.post('/blacklist', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("blacklist projectName");
            let number = req.body.number;
            let query = number * 15;
            Projectname.find({
                    inProcess: false
                }).skip(query).limit(15)
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// deactivate Project
router.post('/deactivate', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log('deactivate Project');
            Projectname.updateOne({
                _id: ObjectId(req.body.id)
            }, {
                $set: {
                    inProcess: false
                }
            }, (err, data) => {
                if (err) {
                    res.json({
                        error: true,
                        message: err
                    })
                } else {
                    Assignedproject.updateMany({
                        projectId: ObjectId(req.body.id)
                    }, {
                        $set: {
                            isActive: false
                        }
                    }, (err, data) => {
                        if (err) {
                            res.json({
                                error: true,
                                message: err
                            })
                        } else {
                            res.json({
                                error: false,
                                message: "Success"
                            })
                        }
                    })
                }
            })
        }
    })
})

// activate Project
router.post('/activate', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activate project")
            Projectname.updateOne({
                    _id: ObjectId(req.body.id)
                }, {
                    $set: {
                        inProcess: true
                    }
                }).then(data => {
                    res.json({
                        error: false,
                        message: "Success"
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})


// count Projectname
router.post('/count', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("count Projectname");
            Projectname.aggregate([{
                        $match: {
                            inProcess: true
                        }
                    },
                    {
                        $count: "count"
                    }
                ])
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// get One Project
router.post('/getOneProject', (req, res) => {
    console.log("getOneProject");
    Projectname.findOne({
            _id: ObjectId(req.body.id)
        })
        .then(data => {
            res.json({
                error: false,
                data: data
            })
        })
        .catch(err => {
            res.json({
                error: true,
                message: err
            })
        })
})

// find manager with respect to project
router.post('/managerRespectProject', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("managerRespectProject");
            Assignedproject.aggregate([{
                $match: {
                    $and: [{
                            projectId: ObjectId(req.body.projectId)
                        },
                        {
                            isActive: true
                        }
                    ]

                }
            }, {
                $lookup: {
                    from: "projectmanagers",
                    localField: "managerId",
                    foreignField: "_id",
                    as: "managerData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        "error": true,
                        "message": err
                    })
                } else {
                    res.json({
                        "error": false,
                        "data": data
                    })
                }
            })
        }
    })
})

// find manager with respect to project Deactivated
router.post('/deactiveManagerRespectProject', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("deactiveManagerRespectProject");
            Assignedproject.aggregate([{
                $match: {
                    $and: [{
                            projectId: ObjectId(req.body.projectId)
                        },
                        {
                            isActive: false
                        }
                    ]

                }
            }, {
                $lookup: {
                    from: "projectmanagers",
                    localField: "managerId",
                    foreignField: "_id",
                    as: "managerData"
                }
            }], (err, data) => {
                if (err) {
                    res.json({
                        "error": true,
                        "message": err
                    })
                } else {
                    res.json({
                        "error": false,
                        "data": data
                    })
                }
            })
        }
    })
})

// de Link manager and Project
router.post('/deLinkManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("delinkManger");
            Assignedproject.updateOne({
                    projectId: ObjectId(req.body.projectId),
                    managerId: ObjectId(req.body.managerId)
                }, {
                    $set: {
                        isActive: false
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// Activate manager and Project
router.post('/activateLinkManager', verifyToken, (req, res) => {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("activatelinkManger");
            Assignedproject.updateOne({
                    projectId: ObjectId(req.body.projectId),
                    managerId: ObjectId(req.body.managerId)
                }, {
                    $set: {
                        isActive: true
                    }
                })
                .then(data => {
                    res.json({
                        error: false,
                        data: data
                    })
                })
                .catch(err => {
                    res.json({
                        error: true,
                        message: err
                    })
                })
        }
    })
})

// project name update
router.post('/updateProjectName', verifyToken, (req,res) =>{
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        } else {
            console.log("updateProjectName");
            Projectname.updateOne({
                _id:ObjectId(req.body.id)
            },{
                $set:{
                    name:req.body.name
                }
            })
            .then(data => {
                res.json({
                    error:false,
                    data:data
                })
            })
            .catch(err => {
                res.json({
                    error:true,
                    message:err
                })
            })
        }
    })
})