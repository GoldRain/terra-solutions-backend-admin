import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { QuickCreateComponent } from './quick-create/quick-create.component';
import { AssociateComponent } from './associate/associate.component';
import { ProjectManagerComponent } from './project-manager/project-manager.component';
import { MachineComponent } from './machine/machine.component';
import { OperatorComponent } from './operator/operator.component';
import { ProjectNameComponent } from './project-name/project-name.component';
import { ProjectManagerDetailComponent } from './project-manager-detail/project-manager-detail.component';
import { OperaterDetailComponent } from './operater-detail/operater-detail.component';
import { ProjectNameDetailComponent } from './project-name-detail/project-name-detail.component';
import { MachineDetailComponent } from './machine-detail/machine-detail.component';
import { MachineRecordComponent } from './machine-record/machine-record.component';
import { MachineDataDetailComponent } from './machine-data-detail/machine-data-detail.component';
import { BlacklistComponent } from './blacklist/blacklist.component';

const routes: Routes = [
  {
    path : 'dashboard',
    component : DashboardComponent
  },
  {
    path : 'register',
    component : RegisterComponent
  },
  {
    path : 'quick-create',
    component : QuickCreateComponent
  },
  {
    path : 'associate',
    component : AssociateComponent
  },
  {
    path : 'project-manager',
    component : ProjectManagerComponent
  },
  {
    path : 'machine',
    component : MachineComponent
  },
  {
    path : 'operator',
    component : OperatorComponent
  },
  {
    path : 'project-name',
    component : ProjectNameComponent
  },
  {
    path : 'project-manager-detail',
    component : ProjectManagerDetailComponent
  },
  {
    path : 'operater-detail',
    component : OperaterDetailComponent
  },
  {
    path : 'project-name-detail',
    component : ProjectNameDetailComponent
  },
  {
    path : 'machine-detail',
    component : MachineDetailComponent
  },
  {
    path : 'machine-record',
    component : MachineRecordComponent
  },
  {
    path : 'machine-data-detail',
    component : MachineDataDetailComponent
  },
  {
    path : 'blacklist',
    component : BlacklistComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
