import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-manager',
  templateUrl: './project-manager.component.html',
  styleUrls: ['./project-manager.component.scss']
})
export class ProjectManagerComponent implements OnInit {

  nameFilter;

  constructor() { }

  ngOnInit() {
  }

  filter(){
    console.log("filter")
  }

  reset(){
    console.log("reset")
  }

  decPage(){
    console.log("dec")
  }

  incPage(){
    console.log("inc")
  }

}
