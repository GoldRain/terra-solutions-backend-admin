import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-machine-data-detail',
  templateUrl: './machine-data-detail.component.html',
  styleUrls: ['./machine-data-detail.component.scss']
})
export class MachineDataDetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }

}
