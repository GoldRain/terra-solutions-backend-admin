import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineDataDetailComponent } from './machine-data-detail.component';

describe('MachineDataDetailComponent', () => {
  let component: MachineDataDetailComponent;
  let fixture: ComponentFixture<MachineDataDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineDataDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineDataDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
