import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-machine-detail',
  templateUrl: './machine-detail.component.html',
  styleUrls: ['./machine-detail.component.scss']
})
export class MachineDetailComponent implements OnInit {
  projectArr=[
    {value:"hello",viewValue:"HELLO"},
    {value:"hey",viewValue:"HEY"}
  ]

  fruits=["hey","hello","what"]

  constructor() { }

  ngOnInit() {
  }

  loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }


}
