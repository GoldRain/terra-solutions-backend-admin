import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-manager-detail',
  templateUrl: './project-manager-detail.component.html',
  styleUrls: ['./project-manager-detail.component.scss']
})
export class ProjectManagerDetailComponent implements OnInit {

  projectArr=[
    {value:"hello",viewValue:"HELLO"},
    {value:"hey",viewValue:"HEY"}
  ]

  fruits=["hey","hello","what"]

  constructor() { }

  ngOnInit() {
  }

  loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }

}
