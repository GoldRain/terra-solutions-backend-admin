import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-name-detail',
  templateUrl: './project-name-detail.component.html',
  styleUrls: ['./project-name-detail.component.scss']
})
export class ProjectNameDetailComponent implements OnInit {
  projectArr=[
    {value:"hello",viewValue:"HELLO"},
    {value:"hey",viewValue:"HEY"}
  ]

  fruits=["hey","hello","what"]

  constructor() { }

  ngOnInit() {
  }

  loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }


}
