import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectNameDetailComponent } from './project-name-detail.component';

describe('ProjectNameDetailComponent', () => {
  let component: ProjectNameDetailComponent;
  let fixture: ComponentFixture<ProjectNameDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectNameDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectNameDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
