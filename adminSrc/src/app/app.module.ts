import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { 
  NbThemeModule, 
  NbLayoutModule,
  NbSidebarModule,
  NbCardModule,
  NbButtonModule,
  NbSpinnerModule,
  NbUserModule,
  NbInputModule,
  NbListModule,
} from '@nebular/theme';
import { MaterialModule } from './material';

import { FormsModule } from '@angular/forms';

import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { QuickCreateComponent } from './quick-create/quick-create.component';
import { AssociateComponent } from './associate/associate.component';
import { ProjectManagerComponent } from './project-manager/project-manager.component';
import { MachineComponent } from './machine/machine.component';
import { OperatorComponent } from './operator/operator.component';
import { ProjectNameComponent } from './project-name/project-name.component';
import { ProjectManagerDetailComponent } from './project-manager-detail/project-manager-detail.component';
import { OperaterDetailComponent } from './operater-detail/operater-detail.component';
import { ProjectNameDetailComponent } from './project-name-detail/project-name-detail.component';
import { MachineDetailComponent } from './machine-detail/machine-detail.component';
import { MachineRecordComponent } from './machine-record/machine-record.component';
import { MachineDataDetailComponent } from './machine-data-detail/machine-data-detail.component';
import { BlacklistComponent } from './blacklist/blacklist.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    RegisterComponent,
    QuickCreateComponent,
    AssociateComponent,
    ProjectManagerComponent,
    MachineComponent,
    OperatorComponent,
    ProjectNameComponent,
    ProjectManagerDetailComponent,
    OperaterDetailComponent,
    ProjectNameDetailComponent,
    MachineDetailComponent,
    MachineRecordComponent,
    MachineDataDetailComponent,
    BlacklistComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbSidebarModule.forRoot(),
    NbCardModule,
    NbButtonModule,
    NbSpinnerModule,
    NbUserModule,
    NbInputModule,
    NbListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
