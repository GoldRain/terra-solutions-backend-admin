import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quick-create',
  templateUrl: './quick-create.component.html',
  styleUrls: ['./quick-create.component.scss']
})
export class QuickCreateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }

}
