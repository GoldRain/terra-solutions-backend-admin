import { Component } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'terr-admin';

  constructor(private sidebarService: NbSidebarService) {
  }

  routLink=[
    '/dashboard',
    '/register',
    '/quick-create',
    '/associate',
    '/project-manager',
    '/machine',
    '/operator',
    '/project-name',
    '/blacklist',
    '/project-manager-detail',
    '/operater-detail',
    '/project-name-detail',
    '/machine-detail',
    '/machine-record',
    '/machine-data-detail'
  ]

  groups=[
    'Dashboard',
    'Register',
    'Quick Create',
    'Associate',
    'Project Manager',
    'Machine',
    'Operater',
    'Project Name',
    'Blacklist',
    'Project Manager detail*',
    'Operater Detail*',
    'Project Name Detail*',
    'Machine Detail*',
    'Machine Record*',
    'Machine Data Detail*'
  ]
  icons=[
    "icon ion-md-planet",
    "icon ion-md-checkmark-circle-outline",
    "icon ion-md-build",
    "icon ion-md-aperture",
    "icon ion-md-leaf",
    "icon ion-md-cog",
    "icon ion-md-person",
    "icon ion-md-nuclear",
    "icon ion-md-warning"
  ]

  toggle() {
    this.sidebarService.toggle(false);
    return false;
  }

}
