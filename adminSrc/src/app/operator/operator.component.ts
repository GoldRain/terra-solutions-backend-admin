import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-operator',
  templateUrl: './operator.component.html',
  styleUrls: ['./operator.component.scss']
})
export class OperatorComponent implements OnInit {

  nameFilter;

  constructor() { }

  ngOnInit() {
  }

  filter(){
    console.log("filter")
  }

  reset(){
    console.log("reset")
  }

  decPage(){
    console.log("dec")
  }

  incPage(){
    console.log("inc")
  }

}
