import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-project-name',
  templateUrl: './project-name.component.html',
  styleUrls: ['./project-name.component.scss']
})
export class ProjectNameComponent implements OnInit {

  nameFilter;

  constructor() { }

  ngOnInit() {
  }

  filter(){
    console.log("filter")
  }

  reset(){
    console.log("reset")
  }

  decPage(){
    console.log("dec")
  }

  incPage(){
    console.log("inc")
  }

}
