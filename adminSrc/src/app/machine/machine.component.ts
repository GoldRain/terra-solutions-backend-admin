import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit {
  nameFilter;

  constructor() { }

  ngOnInit() {
  }

  filter(){
    console.log("filter")
  }

  reset(){
    console.log("reset")
  }

  decPage(){
    console.log("dec")
  }

  incPage(){
    console.log("inc")
  }


}
