import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-operater-detail',
  templateUrl: './operater-detail.component.html',
  styleUrls: ['./operater-detail.component.scss']
})
export class OperaterDetailComponent implements OnInit {


  projectArr=[
    {value:"hello",viewValue:"HELLO"},
    {value:"hey",viewValue:"HEY"}
  ]

  fruits=["hey","hello","what"]

  constructor() { }

  ngOnInit() {
  }

  loading = false;

  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => this.loading = false, 3000);
  }

}
