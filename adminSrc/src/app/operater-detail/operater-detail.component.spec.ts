import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperaterDetailComponent } from './operater-detail.component';

describe('OperaterDetailComponent', () => {
  let component: OperaterDetailComponent;
  let fixture: ComponentFixture<OperaterDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperaterDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperaterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
