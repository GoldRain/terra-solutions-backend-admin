import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineRecordComponent } from './machine-record.component';

describe('MachineRecordComponent', () => {
  let component: MachineRecordComponent;
  let fixture: ComponentFixture<MachineRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MachineRecordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
