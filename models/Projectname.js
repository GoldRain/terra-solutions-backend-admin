const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectnameSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    inProcess:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:String,
        default:Date.now()
    }
})

mongoose.model('projectnames',ProjectnameSchema)