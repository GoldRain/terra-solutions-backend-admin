const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MachineSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    active:{
        type:Boolean,
        default:true
    },
    status:{
        type:String,
        default:"Standby"
    }
})

mongoose.model('machines',MachineSchema)