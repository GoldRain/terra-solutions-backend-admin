const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MachinedataSchema = new Schema({
    machineId:{
        type:Object,
        required:true
    },
    machineName:{
        type:String,
        default:""
    },
    projectId:{
        type:Object,
        required:true
    },
    projectName:{
        type:String,
        default:""
    },
    operaterId:{
        type:Object,
        required:true
    },
    operaterName:{
        type:String,
        default:""
    },
    projectManagerId:{
        type:Object,
        required:true
    },
    projectManagerName:{
        type:String,
        default:"",
    },
    status:{
        type:String,
        default:""
    },
    created_at:{
        type:String,
        default:Date.now()
    },
    location:{
        type:{
            type:String,
            default:"Point"
        },
        coordinates:[0,0]
    },
    place:{
        type:String,
        default:""
    },
    hourmeter:{
        type:String,
        default:""
    },
    image:{
        type:String,
        default:""
    },
    clean:{
        type:Boolean,
        default:false
    },
    greased:{
        type:Boolean,
        default:false
    },
    engineOil:{
        type:String,
        default:""
    },
    hydraulicOil:{
        type:String,
        default:""
    },
    fuel:{
        type:String,
        default:""
    },
    damage:{
        type:Boolean,
        default:false
    },
    noise:{
        type:Boolean,
        default:false
    },
    description:{
        type:String,
        default:""
    },
    damageImg:{
        type:String,
        default:""
    },
    description_damage:{
        type:String,
        default:""
    },
    repaired:{
        type:Boolean,
        default:false
    }
})

mongoose.model("machinedatas",MachinedataSchema);