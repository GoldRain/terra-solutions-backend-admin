const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AssignedoperaterSchema = new Schema({
    operaterId:{
        type:Object,
        required:true
    },
    machineId:{
        type:Object,
        required:true
    },
    isActive:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:String,
        default:Date.now()
    }
})

mongoose.model('assignedoperaters',AssignedoperaterSchema);