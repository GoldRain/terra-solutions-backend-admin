const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OperaterSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    isActive:{
        type:Boolean,
        default:true
    }
})

mongoose.model('operaters',OperaterSchema);