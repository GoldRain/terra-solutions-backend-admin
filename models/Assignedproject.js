const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AssignedprojectSchema = new Schema({
    projectId:{
        type:Object,
        required:true
    },
    managerId:{
        type:Object,
        required:true
    },
    isActive:{
        type:Boolean,
        default:true
    },
    created_at:{
        type:String,
        default:Date.now()
    }
})

mongoose.model('assignedprojects',AssignedprojectSchema);