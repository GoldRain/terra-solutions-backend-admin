const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectmanagerSchema = new Schema({
    name:{ type:String, required:true },
    email:{ type:String, required:true },
    password:{ type:String, required:true },
    isBlocked:{ type:Boolean, default:false }
})

mongoose.model('projectmanagers',ProjectmanagerSchema)